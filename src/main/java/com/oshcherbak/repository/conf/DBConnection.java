package com.oshcherbak.repository.conf;

import com.mysql.cj.jdbc.MysqlDataSource;
import com.oshcherbak.repository.contants.DBConstants;
import com.oshcherbak.service.Mapper;
import org.apache.log4j.Logger;

import java.sql.*;

public class DBConnection {
    Logger logger = Logger.getLogger(this.getClass());

    MysqlDataSource ds;
    private static DBConnection dbConnection;

    private DBConnection(){
        ds = new MysqlDataSource();
        ds.setServerName(DBConstants.HOST);
        ds.setPortNumber(DBConstants.PORT);
        ds.setDatabaseName(DBConstants.DB_NAME);
        ds.setUser(DBConstants.USER);
        ds.setPassword(DBConstants.PASSWORD);
    }

    public static Connection getConnection() throws SQLException {
        if (dbConnection == null){
            dbConnection = new DBConnection();
        }
        return dbConnection.ds.getConnection();
    }

    public static DBConnection getDBConnection() throws SQLException {
        if (dbConnection == null){
            dbConnection = new DBConnection();
        }
        return dbConnection;
    }

    public <T> T find(String sql, Mapper<T> mapper) throws SQLException{
        try (
            Connection connection = DBConnection.getConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sql)){

            if (resultSet.next()) {
                return mapper.map(resultSet);
            } else {
                logger.fatal("find hasn't any result / sql query ?= " + sql);
            }
        } catch (SQLException e) {
            logger.error(this.getClass().toString() + "::find sql error ", e);
        }
        return null;
    }

    public boolean update(String sql) throws SQLException{
        Connection connection = null;
        int resultValue = 0;
        Statement statement = null;
        try {
            connection = DBConnection.getConnection();
            statement = connection.createStatement();
            resultValue = statement.executeUpdate(sql);
        } catch (SQLException e) {
            logger.fatal(this.getClass().toString() + "::update sql error ", e);
        } finally {
            if (statement != null) {
                statement.close();
            }
            if (connection != null) {
                connection.close();
            }
        }
        if (resultValue > 0) {
            return true;
        }
        return false;
    }
}
