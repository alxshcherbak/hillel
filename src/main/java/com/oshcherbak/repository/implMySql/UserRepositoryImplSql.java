package com.oshcherbak.repository.implMySql;

import com.oshcherbak.model.User;
import com.oshcherbak.model.UserRole;
import com.oshcherbak.repository.UserRepository;
import com.oshcherbak.repository.conf.DBConnection;
import org.apache.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserRepositoryImplSql implements UserRepository{
    Logger logger = Logger.getLogger(this.getClass());

    @Override
    public User findOne(int id){
        User user = null;
        try {
            String QUERY ="SELECT u.id, u.name, u.surname, u.login, u.role\n" +
                    "FROM Users u\n" +
                    "WHERE u.id = "  + id;
                    user = DBConnection.getDBConnection().find(QUERY,
                    resultSet -> getUserFromResultSet(resultSet)
                );
            } catch (SQLException e) {
            logger.fatal(this.getClass().toString() + "::find sql error ", e);
            }
        return user;
    }

    @Override
    public List<User> findList(int[] ids) {
        if (ids == null || ids.length == 0){
            return null;
        }

        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT u.id, u.name, u.surname, u.login, u.role\n" +
                    "FROM Users u\n" +
                    "WHERE u.id IN (" + ids[0]);
            for (int i = 1; i < ids.length; i++){
                sql.append(", " + ids[i]);
            }
            sql.append(")");


            return DBConnection.getDBConnection().find(
                      sql.toString(),
                    resultSet -> {
                          List<User> userList = new ArrayList<>();
                          do {
                              userList.add(getUserFromResultSet(resultSet));
                          } while (resultSet.next());
                          return userList;
                    }
            );
        } catch (SQLException e) {
            logger.fatal(this.getClass().toString() + ":: sql error ", e);
        }
        return null;
    }

    @Override
    //TODO
    public void update(User user) {

    }

    @Override
    //TODO
    public void update(List<User> users) {

    }

    static User getUserFromResultSet(ResultSet rs) throws SQLException {
        return new User(rs.getInt("u.id"),
                rs.getString("u.name"),
                rs.getString("u.surname"),
                rs.getString("u.login"),
                UserRole.valueOf(rs.getString("u.role")));
//                UserRole.getRole(rs.getString("Users.role")));
    }
}
