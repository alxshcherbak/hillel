package com.oshcherbak.repository.implMySql;

import com.oshcherbak.model.*;
import com.oshcherbak.model.proxy.UserProxy;
import com.oshcherbak.repository.TicketRepository;
import com.oshcherbak.repository.conf.DBConnection;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TicketRepositoryImplSql implements TicketRepository {
    Logger logger = Logger.getLogger(this.getClass());

    @Override
    public Ticket findOne(int id) {
        Ticket ticket = null;
            try {
                String QUERY = "SELECT t.id, t.title, t.description, t.author, t.assigment, " +
                        "  t.create_date, t.last_change_date, t.status, t.type\n" +
                        "FROM Tickets t\n" +
                        "WHERE t.id = "  + id;
                ticket = DBConnection.getDBConnection().find(QUERY,
                        resultSet -> getTicketFromResultSet(resultSet));
            } catch (SQLException e) {
                logger.fatal(this.getClass().toString() + "::find sql error ", e);
            }
        return ticket;
    }

    @Override
    public Ticket findWithAliases(int id) {
        Ticket ticket = null;
        try {
            String QUERY = "SELECT t.id, t.title, t.description, t.author, t.assigment, t.create_date,\n" +
                    "  t.last_change_date, t.status, t.type, u.id, u.name, u.surname, u.login, u.role FROM Tickets t\n" +
                    "  JOIN Users u ON (t.author = u.id OR t.assigment = u.id) AND t.id = " + id;

            ticket = DBConnection.getDBConnection().find(QUERY,
                    resultSet -> {
                        Ticket ticketT = getTicketFromResultSet(resultSet);

                            User[] users = new User[2];
                            users[0] = UserRepositoryImplSql.getUserFromResultSet(resultSet);
                            resultSet.next();
                            users[1] =UserRepositoryImplSql.getUserFromResultSet(resultSet);

                        for (User user : users){
                            if (user != null){
                                if (user.getId() == ticketT.getAuthor().getId()){
                                    ticketT.setAuthor(user);
                                } else if (user.getId() == ticketT.getAssigment().getId()){
                                    ticketT.setAssigment(user);
                                }
                            }
                        }

                        return ticketT;
                    });
        } catch (SQLException e) {
            logger.fatal(this.getClass().toString() + "::find sql error ", e);
        }
        return ticket;
    }

    @Override
    public List<Ticket> findTicketsByAssigment(int userAssigmentId) {
        try {
            String QUERY = "SELECT t.id, t.title, t.description, t.author, t.assigment, t.create_date,\n" +
                    "t.last_change_date, t.status, t.type\n" +
                    "FROM Tickets t\n" +
                    "WHERE t.assigment = " + userAssigmentId;
            return DBConnection.getDBConnection().find(QUERY,
                    resultSet -> {
                        List<Ticket> list = new ArrayList<>();
                        do {
                            list.add(getTicketFromResultSet(resultSet));
                        } while (resultSet.next());
                        return list;
                    });
        } catch (SQLException e) {
            logger.fatal(this.getClass().toString() + "::find sql error ", e);
        }
        return null;
    }

    @Override
    public List<Ticket> findTicketsByAuthor(int userAuthorId) {
        try {
            String QUERY = "SELECT t.id, t.title, t.description, t.author, t.assigment, t.create_date,\n" +
                    "t.last_change_date, t.status, t.type\n" +
                    "FROM Tickets t\n" +
                    "WHERE t.author = " + userAuthorId;
            return DBConnection.getDBConnection().find(QUERY,
                    resultSet -> {
                        List<Ticket> list = new ArrayList<>();
                        do {
                            list.add(getTicketFromResultSet(resultSet));
                        } while (resultSet.next());
                        return list;
                    });
        } catch (SQLException e) {
            logger.fatal(this.getClass().toString() + "::find sql error ", e);
        }
        return null;
    }

    @Override
    public int updateTicket(Connection connection, Ticket ticket) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(
                "UPDATE Tickets SET Tickets.title = ?, Tickets.description = ?, Tickets.author = ?,\n" +
                        "  Tickets.assigment = ?, Tickets.type = ?, Tickets.status = ?,\n" +
                        "  Tickets.create_date = ?, Tickets.last_change_date = current_time\n" +
                        "WHERE Tickets.id = ?");

        preparedStatement.setString(1, ticket.getTitle());
        preparedStatement.setString(2, ticket.getDescription());
        preparedStatement.setInt(3, ticket.getAuthor().getId());
        preparedStatement.setInt(4, ticket.getAssigment().getId());
        preparedStatement.setString(5, ticket.getType().name());
        preparedStatement.setString(6, ticket.getStatus().name());
        preparedStatement.setTimestamp(7, new Timestamp(ticket.getCreateDate().getTime()));
        preparedStatement.setInt(8, ticket.getId());

        return preparedStatement.executeUpdate();
    }

    static Ticket getTicketFromResultSet(ResultSet rs) throws SQLException {
        return new Ticket(rs.getInt("t.id"),
                new UserProxy(rs.getInt("t.author")),
                rs.getString("t.title"),
                rs.getString("t.description"),
                new UserProxy(rs.getInt("t.author")),
                rs.getTimestamp("t.create_date"),
                rs.getTimestamp("t.last_change_date"),
                TicketStatus.valueOf(rs.getString("t.status")),
                TicketType.valueOf(rs.getString("t.type")));
    }
}
