package com.oshcherbak.repository.implMySql;

import com.oshcherbak.model.Comment;
import com.oshcherbak.model.proxy.TicketProxy;
import com.oshcherbak.model.proxy.UserProxy;
import com.oshcherbak.repository.CommentRepository;
import com.oshcherbak.repository.conf.DBConnection;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CommentRepositoryImplSql implements CommentRepository {
    Logger logger = Logger.getLogger(this.getClass());

    @Override
    public Comment findCommentClear(int id) {
        try {
            String QUERY = "SELECT c.id, c.author, c.ticket, c.body, c.date\n" +
                    "FROM Comments c\n" +
                    "WHERE c.id = "  + id;
            return DBConnection.getDBConnection().find(QUERY,
                    resultSet -> getCommentFromResultSet(resultSet));
        } catch (SQLException e) {
            logger.fatal(this.getClass().toString() + "::findCommentClear sql error ", e);
        }
        return null;
    }

    @Override
    public Comment findCommentWithAliases(int id) {
        try {
            String QUERY = "SELECT c.id, c.author, c.ticket, c.body, c.date,\n" +
                    "  t.id, t.title, t.description, t.author, t.assigment,\n" +
                    "  t.create_date, t.last_change_date, t.status, t.type,\n" +
                    "  u.id, u.name, u.surname, u.login, u.role FROM Comments c\n" +
                    "  JOIN Users u ON c.author = u.id  AND c.id = " + id + " \n" +
                    "  JOIN Tickets t ON c.ticket = t.id AND c.id = " + id;
            return DBConnection.getDBConnection().find(QUERY,
                    resultSet -> {
                Comment comment = getCommentFromResultSet(resultSet);
                comment.setAuthor(UserRepositoryImplSql.getUserFromResultSet(resultSet));
                comment.setTicket(TicketRepositoryImplSql.getTicketFromResultSet(resultSet));
                return comment;});
        } catch (SQLException e) {
            logger.fatal(this.getClass().toString() + "::findCommentWithAliases sql error ", e);
        }
        return null;
    }

    @Override
    public List<Comment> findCommentsByTicket(int ticketId) {
        try {
            String QUERY = "SELECT c.id, c.author, c.ticket, c.body, c.date\n" +
                    "FROM Comments c\n" +
                    "WHERE c.ticket = "  + ticketId;
            return DBConnection.getDBConnection().find(QUERY,
                    resultSet -> {
                List<Comment> commentList = new ArrayList<>();
                do {
                    commentList.add(getCommentFromResultSet(resultSet));
                } while (resultSet.next());
                return commentList;});
        } catch (SQLException e) {
            logger.fatal(this.getClass().toString() + "::findCommentsByTicket sql error ", e);
        }
        return null;
    }

    @Override
    public List<Comment> findCommentsByTicketWithAliases(int ticketId) {
        try {
            String QUERY = "SELECT c.id, c.author, c.ticket, c.body, c.date,\n" +
                    "  t.id, t.title, t.description, t.author, t.assigment,\n" +
                    "  t.create_date, t.last_change_date, t.status, t.type,\n" +
                    "  u.id, u.name, u.surname, u.login, u.role FROM Comments c\n" +
                    "  JOIN Users u ON c.author = u.id  AND c.ticket = " + ticketId + " \n" +
                    "  JOIN Tickets t ON c.ticket = t.id AND c.ticket = " + ticketId;
            return DBConnection.getDBConnection().find(QUERY,
                    resultSet -> {
                        List<Comment> commentList = new ArrayList<>();
                        do {
                            Comment comment = getCommentFromResultSet(resultSet);
                            comment.setAuthor(UserRepositoryImplSql.getUserFromResultSet(resultSet));
                            comment.setTicket(TicketRepositoryImplSql.getTicketFromResultSet(resultSet));
                            commentList.add(comment);
                        } while (resultSet.next());
                        return commentList;});
        } catch (SQLException e) {
            logger.fatal(this.getClass().toString() + "::findCommentsByTicketWithAliases sql error ", e);
        }
        return null;
    }

    @Override
    public List<Comment> findCommentsByAuthor(int userId) {
        try {
            String QUERY = "SELECT c.id, c.author, c.ticket, c.body, c.date\n" +
                    "FROM c\n" +
                    "WHERE c.author = "  + userId;
            return DBConnection.getDBConnection().find(QUERY,
                    resultSet -> {
                        List<Comment> commentList = new ArrayList<>();
                        do {
                            commentList.add(getCommentFromResultSet(resultSet));
                        } while (resultSet.next());
                        return commentList;});
        } catch (SQLException e) {
            logger.fatal(this.getClass().toString() + "::findCommentsByAuthor sql error ", e);
        }
        return null;
    }

    @Override
    public List<Comment> findCommentsByAuthorWithAliases(int userId) {
        try {
            String QUERY = "SELECT c.id, c.author, c.ticket, c.body, c.date,\n" +
                    "  t.id, t.title, t.description, t.author, t.assigment,\n" +
                    "  t.create_date, t.last_change_date, t.status, t.type,\n" +
                    "  u.id, u.name, u.surname, u.login, u.role FROM Comments c\n" +
                    "  JOIN Users u ON c.author = u.id  AND c.author = " + userId + " \n" +
                    "  JOIN Tickets t ON c.ticket = t.id AND c.author = " + userId;
            return DBConnection.getDBConnection().find(QUERY,
                    resultSet -> {
                        List<Comment> commentList = new ArrayList<>();
                        do {
                            Comment comment = getCommentFromResultSet(resultSet);
                            comment.setAuthor(UserRepositoryImplSql.getUserFromResultSet(resultSet));
                            comment.setTicket(TicketRepositoryImplSql.getTicketFromResultSet(resultSet));
                            commentList.add(comment);
                        } while (resultSet.next());
                        return commentList;});
        } catch (SQLException e) {
            logger.fatal(this.getClass().toString() + "::findCommentsByAuthorWithAliases sql error ", e);
        }
        return null;
    }

    @Override
    public int addComment(Connection connection, Comment comment, int authorId, int ticketId) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO Comments (author, ticket, date, body)\n" +
                "VALUES (?, ?, CURRENT_TIME , ?)");
        preparedStatement.setInt(1, comment.getAuthor().getId());
        preparedStatement.setInt(2, comment.getTicket().getId());
        preparedStatement.setString(3, comment.getBody() );
        return preparedStatement.executeUpdate();
    }

    static Comment getCommentFromResultSet(ResultSet rs) throws SQLException {
        return new Comment(rs.getInt("c.id"),
                new UserProxy(rs.getInt("c.author")),
                new TicketProxy(rs.getInt("c.ticket")),
                rs.getTimestamp("c.date"),
                rs.getString("c.body"));
    }
}
