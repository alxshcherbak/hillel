package com.oshcherbak.repository.contants;

public interface DBConstants {
    String HOST = "localhost";
    int PORT = 3306;
    String DB_NAME = "jira_v2.0";
    String USER = "root";
    String PASSWORD = "root";
}
