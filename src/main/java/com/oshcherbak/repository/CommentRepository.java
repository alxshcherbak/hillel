package com.oshcherbak.repository;

import com.oshcherbak.model.Comment;
import com.oshcherbak.model.Ticket;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public interface CommentRepository {

    public Comment findCommentClear(int id);
    public Comment findCommentWithAliases(int id);

    public List<Comment> findCommentsByTicket(int ticketId);
    public List<Comment> findCommentsByTicketWithAliases(int ticketId);

    public List<Comment> findCommentsByAuthor(int userId);
    public List<Comment> findCommentsByAuthorWithAliases(int userId);

    public int addComment(Connection connection, Comment comment, int authorId, int ticketId) throws SQLException;
}
