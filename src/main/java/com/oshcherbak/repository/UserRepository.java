package com.oshcherbak.repository;

import com.oshcherbak.model.User;

import java.util.List;

public interface UserRepository {
    User findOne(int id);
    List<User> findList(int... ids);

    void update(User user);
    void update(List<User> users);
}
