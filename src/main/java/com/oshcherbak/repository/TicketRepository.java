package com.oshcherbak.repository;

import com.oshcherbak.model.Ticket;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public interface TicketRepository {
    Ticket findOne(int id);
    Ticket findWithAliases(int id);

    List<Ticket> findTicketsByAssigment(int userAssigmentId);
    List<Ticket> findTicketsByAuthor(int userAuthorId);

    int updateTicket(Connection connection, Ticket id) throws SQLException;
}
