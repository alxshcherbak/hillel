package com.oshcherbak.service;

import com.oshcherbak.model.Ticket;
import com.oshcherbak.model.User;

import java.util.List;

public interface TicketService {
    public Ticket getTicketClear(int id);
    public Ticket getTicketWithAliases(int id);

    public List<Ticket> getTicketsByAssigment(User user);
    public List<Ticket> getTicketsByAuthor(User user);
}
