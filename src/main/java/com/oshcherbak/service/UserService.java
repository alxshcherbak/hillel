package com.oshcherbak.service;

import com.oshcherbak.model.User;

public interface UserService {
    User getUserById(int userId);
}
