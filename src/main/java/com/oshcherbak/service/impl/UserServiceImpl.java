package com.oshcherbak.service.impl;

import com.oshcherbak.model.User;
import com.oshcherbak.service.UserService;

public class UserServiceImpl implements UserService {

    @Override
    public User getUserById(int userId){
       return ServiceRepository.getUserRepository().findOne(userId);
    }
}
