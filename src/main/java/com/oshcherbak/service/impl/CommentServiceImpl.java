package com.oshcherbak.service.impl;

import com.oshcherbak.model.Comment;
import com.oshcherbak.model.Ticket;
import com.oshcherbak.model.User;
import com.oshcherbak.repository.conf.DBConnection;
import com.oshcherbak.service.CommentService;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class CommentServiceImpl implements CommentService {
    Logger logger = Logger.getLogger(this.getClass());

    @Override
    public Comment findOne(int id) {
        return ServiceRepository.getCommentRepository().findCommentClear(id);
    }

    @Override
    public List<Comment> findByAuthor(int authorId) {
        return ServiceRepository.getCommentRepository().findCommentsByAuthor(authorId);
    }

    @Override
    public List<Comment> findByTicket(int ticketId) {
        return ServiceRepository.getCommentRepository().findCommentsByTicket(ticketId);
    }

    @Override
    public Comment findOneFull(int id) {
        return ServiceRepository.getCommentRepository().findCommentWithAliases(id);
    }

    @Override
    public void addComment(Comment comment) {
        Connection connection = null;

        try {
            connection = DBConnection.getConnection();
            connection.setAutoCommit(false);

            ServiceRepository.getCommentRepository().addComment(connection, comment, comment.getAuthor().getId(), comment.getTicket().getId());
            ServiceRepository.getTicketRepository().updateTicket(connection, comment.getTicket());

            connection.commit();

        } catch (SQLException e) {
            logger.fatal(this.getClass().toString() + "::find sql error ", e);
        } finally {
            if (connection!=null){
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.fatal(this.getClass().toString() + "::find sql error ", e);
                }
            }
        }

    }
}
