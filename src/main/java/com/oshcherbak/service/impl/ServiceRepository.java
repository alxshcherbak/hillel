package com.oshcherbak.service.impl;

import com.oshcherbak.repository.CommentRepository;
import com.oshcherbak.repository.TicketRepository;
import com.oshcherbak.repository.UserRepository;
import com.oshcherbak.repository.implMySql.CommentRepositoryImplSql;
import com.oshcherbak.repository.implMySql.TicketRepositoryImplSql;
import com.oshcherbak.repository.implMySql.UserRepositoryImplSql;

public class ServiceRepository {
    private static ServiceRepository repository;

    private UserRepository userRepository;
    private TicketRepository ticketRepository;
    private CommentRepository commentRepository;


    private ServiceRepository(){
        userRepository = new UserRepositoryImplSql();
        ticketRepository = new TicketRepositoryImplSql();
        commentRepository = new CommentRepositoryImplSql();
    }

    static ServiceRepository getRepository(){
        if (repository == null){
            repository = new ServiceRepository();
        }

        return repository;
    }

    static UserRepository getUserRepository(){
        return getRepository().userRepository;
    }

    static TicketRepository getTicketRepository(){
        return getRepository().ticketRepository;
    }

    static CommentRepository getCommentRepository(){
        return getRepository().commentRepository;
    }

}
