package com.oshcherbak.service.impl;

import com.oshcherbak.model.Ticket;
import com.oshcherbak.model.User;
import com.oshcherbak.service.TicketService;

import java.util.List;

public class TicketServiceImpl implements TicketService {
    @Override
    public Ticket getTicketClear(int id) {
        return ServiceRepository.getTicketRepository().findOne(id);
    }

    @Override
    public Ticket getTicketWithAliases(int id) {
        return ServiceRepository.getTicketRepository().findWithAliases(id);
    }

    @Override
    public List<Ticket> getTicketsByAssigment(User user) {
        return ServiceRepository.getTicketRepository().findTicketsByAuthor(user.getId());
    }

    @Override
    public List<Ticket> getTicketsByAuthor(User user) {
        return ServiceRepository.getTicketRepository().findTicketsByAuthor(user.getId());
    }
}
