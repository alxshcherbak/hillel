package com.oshcherbak.service;

import java.sql.*;

@FunctionalInterface
public interface Mapper<T> {
    T map(ResultSet set) throws SQLException;
}
