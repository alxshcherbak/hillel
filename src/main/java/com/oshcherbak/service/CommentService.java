package com.oshcherbak.service;

import com.oshcherbak.model.Comment;
import com.oshcherbak.model.Ticket;
import com.oshcherbak.model.User;

import java.sql.SQLException;
import java.util.List;

public interface CommentService {
    Comment findOne(int id);
    List<Comment> findByAuthor(int authorId);
    List<Comment> findByTicket(int ticketId);

    Comment findOneFull(int id);

    void addComment(Comment comment);
}
