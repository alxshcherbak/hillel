package com.oshcherbak.model;

public enum UserRole {
    DEV, MANAGER, TESTER, SUPPORT, UNKNOWN;

    public static UserRole getRole(String status){
        status.toLowerCase();
        switch (status){
            case "dev": return DEV;
            case "tester": return TESTER;
            case "manager": return MANAGER;
            case "support": return SUPPORT;
            default: return UNKNOWN;
        }
    }
}
