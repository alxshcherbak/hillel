package com.oshcherbak.model;

import java.util.Date;

public class Ticket {
    private int id;
    private User author;
    private String title;
    private String description;
    private User assigment;

    private Date createDate;
    private Date lastUpdate;

    private TicketStatus status;
    private TicketType type;

    public Ticket(User author, String title, String description, User assigment, Date createDate, Date lastUpdate, TicketStatus status, TicketType type) {
        this.author = author;
        this.title = title;
        this.description = description;
        this.assigment = assigment;
        this.createDate = createDate;
        this.lastUpdate = lastUpdate;
        this.status = status;
        this.type = type;
    }

    public Ticket(int id, User author, String title, String description, User assigment, Date createDate, Date lastUpdate, TicketStatus status, TicketType type) {
        this.id = id;
        this.author = author;
        this.title = title;
        this.description = description;
        this.assigment = assigment;
        this.createDate = createDate;
        this.lastUpdate = lastUpdate;
        this.status = status;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public User getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public User getAssigment() {
        return assigment;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public TicketStatus getStatus() {
        return status;
    }

    public TicketType getType() {
        return type;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setAssigment(User assigment) {
        this.assigment = assigment;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public void setStatus(TicketStatus status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "id=" + id +
                ", author=" + author +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", assigment=" + assigment +
                ", createDate=" + createDate +
                ", lastUpdate=" + lastUpdate +
                ", status=" + status +
                ", type=" + type +
                '}';
    }
}
