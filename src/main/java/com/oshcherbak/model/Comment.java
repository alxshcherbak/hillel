package com.oshcherbak.model;

import java.util.Date;

public class Comment {
    private int id;
    private User author = null;
    private Ticket ticket = null;
    private Date date = null;
    private String body = null;

    public int getId() {
        return id;
    }

    public User getAuthor() {
        return author;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public Date getDate() {
        return date;
    }

    public String getBody() {
        return body;
    }

    public Comment(int id, User author, Ticket ticket, Date date, String body) {
        this.id = id;
        this.author = author;
        this.ticket = ticket;
        this.date = date;
        this.body = body;
    }

    public Comment(User author, Ticket ticket, String body) {
        this.author = author;
        this.ticket = ticket;
        this.date = new Date();
        this.body = body;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", author=" + author +
                ", ticket=" + ticket +
                ", date=" + date +
                ", body='" + body + '\'' +
                '}';
    }
}
