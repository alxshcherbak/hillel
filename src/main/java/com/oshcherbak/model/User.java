package com.oshcherbak.model;

import org.hibernate.annotations.Type;

import javax.persistence.*;

@Entity
@Table(name = "Users")
public class User {
    public User() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "name")
    private String name;
    @Column(name = "surname")
    private String surname;
    @Column(name = "login")
    private String login;
//    @Column(name = "role", )
    @Enumerated(EnumType.STRING)
//    @Convert(converter = RoleConverter.class)
    private UserRole role;

    public User(String name, String surname, String login, UserRole role) {
        this.name = name;
        this.surname = surname;
        this.login = login;
        this.role = role;
    }

    public User(int id, String name, String surname, String login, UserRole role) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.login = login;
        this.role = role;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getLogin() {
        return login;
    }

    public UserRole getRole() {
        return role;
    }



    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", login='" + login + '\'' +
                ", role=" + role +
                '}';
    }


    @Converter(autoApply = true)
    public static class RoleConverter implements AttributeConverter<UserRole, Enum> {

//        @Override
//        public String convertToDatabaseColumn(UserRole attribute) {
//            return attribute.name();
//        }

        @Override
        public Enum convertToDatabaseColumn(UserRole attribute) {
            return attribute;
        }

        @Override
        public UserRole convertToEntityAttribute(Enum dbData) {
            return UserRole.getRole(dbData.name());
        }

//        @Override
//        public UserRole convertToEntityAttribute(String dbData) {
//            return UserRole.getRole(dbData);
//        }
    }


}
