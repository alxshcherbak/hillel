package com.oshcherbak.model;

public enum TicketStatus {
    OPEN, IN_PROGRESS, RESOLVED, UNKNOWN;

    public static TicketStatus getStatus(String status){
        status.toLowerCase();
        switch (status){
            case "open": return OPEN;
            case "in progress": return IN_PROGRESS;
            case "resolved": return RESOLVED;
            default: return UNKNOWN;
        }
    }
}
