package com.oshcherbak.model;

public enum  TicketType {
    BUG, ISSUE, SUPPORT, UNKNOWN;

    public static TicketType getType(String status){
        status.toLowerCase();
        switch (status){
            case "bug": return BUG;
            case "issue": return ISSUE;
            case "support": return SUPPORT;
            default: return UNKNOWN;
        }
    }
}
