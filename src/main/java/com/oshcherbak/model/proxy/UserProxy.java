package com.oshcherbak.model.proxy;

import com.oshcherbak.model.User;
import com.oshcherbak.model.UserRole;

public class UserProxy extends User{
    public UserProxy(int id) {
        super(id, null, null, null, UserRole.UNKNOWN);
    }

    @Override
    public String getName() {
        return "user proxy id = " + getId();
    }

    @Override
    public String getSurname() {
        return "user proxy id = " + getId();
    }

    @Override
    public String getLogin() {
        return "user proxy id = " + getId();
    }

    @Override
    public String toString() {
        return "UserProxy{id = " + getId() + "}";
    }
}
