package com.oshcherbak.model.proxy;

import com.oshcherbak.model.Ticket;
import com.oshcherbak.model.TicketStatus;
import com.oshcherbak.model.TicketType;
import com.oshcherbak.model.User;

import java.util.Date;

public class TicketProxy extends Ticket{
    public TicketProxy(int id) {
        super(id, null, null, null, null, null, null, TicketStatus.UNKNOWN, TicketType.UNKNOWN);
    }

    @Override
    public String toString() {
        return "UserProxy{id = " + getId() + "}";
    }
}
