-- MySQL dump 10.13  Distrib 5.7.20, for Linux (x86_64)
--
-- Host: localhost    Database: jira_v2.0
-- ------------------------------------------------------
-- Server version	5.7.20-0ubuntu0.16.04.1

CREATE database `jira_v2.0`;
USE `jira_v2.0`;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Comments`
--

DROP TABLE IF EXISTS `Comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author` int(11) NOT NULL,
  `ticket` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `body` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Comments`
--

LOCK TABLES `Comments` WRITE;
/*!40000 ALTER TABLE `Comments` DISABLE KEYS */;
INSERT INTO `Comments` VALUES (1,1,1,'2017-11-29 21:15:36','comment author 1 ticket 1'),(2,3,1,'2017-11-29 21:16:09','comment author 3 ticket 1'),(3,2,3,'2017-11-29 21:16:40','comment author 2 ticket 2'),(4,1,2,'2017-11-29 21:17:05','comment author 1 ticket 2'),(5,1,1,'2017-11-29 21:17:30','comment author 1 ticket 1 // second'),(6,2,3,'2017-11-29 00:00:00','body commit from terminal'),(7,2,3,'2017-11-29 00:00:00','body commit from terminal'),(8,2,3,'2017-11-29 22:23:23','body commit from terminal'),(11,7,4,'2017-11-29 22:57:11','create comment from test');
/*!40000 ALTER TABLE `Comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tickets`
--

DROP TABLE IF EXISTS `Tickets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Tickets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `description` text,
  `author` int(11) NOT NULL,
  `assigment` int(11) DEFAULT NULL,
  `create_date` datetime NOT NULL,
  `last_change_date` datetime NOT NULL,
  `status` enum('OPEN','IN_PROGRESS','RESOLVED','UNKNOWN') NOT NULL,
  `type` enum('BUG','ISSUE','SUPPORT','UNKNOWN') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tickets`
--

LOCK TABLES `Tickets` WRITE;
/*!40000 ALTER TABLE `Tickets` DISABLE KEYS */;
INSERT INTO `Tickets` VALUES (1,'from console','from console',1,1,'2017-11-29 22:35:45','2017-11-29 22:35:45','OPEN','BUG'),(2,'ISSUE 2','ISSUE 2 desc',4,2,'2017-11-28 19:03:39','2017-11-29 16:03:45','RESOLVED','ISSUE'),(3,'SUPPORT 3','SUPPORT 3 desc ',6,2,'2017-11-27 19:04:45','2017-11-29 11:04:51','RESOLVED','SUPPORT'),(4,'ISSUE 4','ISSUE 4 desc',4,4,'2017-10-29 00:00:00','2017-11-29 22:57:11','RESOLVED','ISSUE');
/*!40000 ALTER TABLE `Tickets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Users`
--

DROP TABLE IF EXISTS `Users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `surname` varchar(50) DEFAULT NULL,
  `login` varchar(50) NOT NULL,
  `role` enum('DEV','TESTER','MANAGER','SUPPORT','UNKNOWN') NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Users_login_uindex` (`login`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Users`
--

LOCK TABLES `Users` WRITE;
/*!40000 ALTER TABLE `Users` DISABLE KEYS */;
INSERT INTO `Users` VALUES (1,'oleksii','shcherbak','os465g','DEV'),(2,'skiba','dmitry','dm862e','SUPPORT'),(3,'aider','der','ad679r','DEV'),(4,'ivan','drago','iv308d','MANAGER'),(5,'drogo','khall','dt410h','DEV'),(6,'druce','wayne','br693w','MANAGER'),(7,'tony','stark','tj777s','DEV'),(8,'volodymir','tuluman','vl451t','TESTER');
/*!40000 ALTER TABLE `Users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-29 22:58:24
