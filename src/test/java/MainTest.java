import com.oshcherbak.model.Comment;
import com.oshcherbak.model.Ticket;
import com.oshcherbak.model.User;
import com.oshcherbak.repository.CommentRepository;
import com.oshcherbak.repository.TicketRepository;
import com.oshcherbak.repository.UserRepository;
import com.oshcherbak.repository.implMySql.CommentRepositoryImplSql;
import com.oshcherbak.repository.implMySql.TicketRepositoryImplSql;
import com.oshcherbak.repository.implMySql.UserRepositoryImplSql;
import com.oshcherbak.service.CommentService;
import com.oshcherbak.service.TicketService;
import com.oshcherbak.service.UserService;
import com.oshcherbak.service.impl.CommentServiceImpl;
import com.oshcherbak.service.impl.TicketServiceImpl;
import com.oshcherbak.service.impl.UserServiceImpl;

public class MainTest {
    public static void main(String[] args) throws Exception {
        UserRepository userRepository = new UserRepositoryImplSql();
        TicketRepository ticketRepository = new TicketRepositoryImplSql();
        CommentRepository commentRepository = new CommentRepositoryImplSql();
        System.out.println(userRepository.findOne(3));
//        System.out.println(userRepository.findList(1,4,2,3).toString());

//        System.out.println(ticketRepository.findOne(3));
//        System.out.println(ticketRepository.findWithAliases(2));
//        System.out.println(commentRepository.findCommentWithAliases(1));

//        CommentService commentService = new CommentServiceImpl();
//        UserService userService = new UserServiceImpl();
//        TicketService ticketService = new TicketServiceImpl();
//
//        Ticket ticket = ticketService.getTicketClear(3);
//        System.out.println("\n" + ticket);
//        System.out.println("\n" + commentService.findByTicket(ticket.getId()));
//        User user = userService.getUserById(7);
//        System.out.println("\n" + user);
//
//        Comment comment = new Comment(user, ticket, "create comment from test");
//
//        commentService.addComment(comment);
//
//        ticket = ticketService.getTicketClear(3);
//        System.out.println("\n" + ticket);
//        System.out.println("\n" + commentService.findByTicket(ticket.getId()));
//
//        System.out.println();
    }
}
